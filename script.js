let button = document.querySelector("button");
let input = document.querySelector("input");
let lista = document.querySelector('ul');

//Para adicionar nome à lista:
button.addEventListener("click", (e) => {
    let elemento = document.createElement('li');
    elemento.innerHTML = input.value;
    lista.append(elemento);
    input.value="";
})

//Para remover nome da lista, apenas clicando em cima do mesmo
lista.addEventListener("click", (e) => { 
    lista.removeChild(e.target) 
})
